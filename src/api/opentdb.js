export const getQuestions = () => {
    return fetch('https://opentdb.com/api.php?amount=10&difficulty=easy')
        .then(res => res.json())
        .then(res => {
            if (res.response_code != 0) {
                console.log('Something happened with the API request');
            }
            return res.results;
        });
}